import { FC } from "react"
import { Task1View } from "views/task1"

export interface Task1PageProps {

}

export const Task1Page: FC<Task1PageProps> = (props: Task1PageProps) => {
    return (
        <>
            <Task1View/>
        </>
    )
}