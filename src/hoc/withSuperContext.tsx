import { FunctionComponent, useContext, useState } from 'react';
import { CustomProvider } from 'rsuite';
import { SuperContext } from 'utils/contexts';

export const withSuperContext = (WrappedComponent: FunctionComponent) => {

    const NewFunctionComponent: FunctionComponent = () => {

        const [height, setHeight] = useState(window.innerHeight)
        const [width, setWidth] = useState(window.innerWidth)

        window.addEventListener('resize', () => {
            const { innerWidth, innerHeight } = window;
            setHeight(innerHeight)
            setWidth(innerWidth)
        })

        return (
            <SuperContext.Provider value={{ screenHeight: height, screenWidth: width }}>
                <CustomProvider theme="dark">
                    <WrappedComponent />
                </CustomProvider>
            </SuperContext.Provider>
        )
    }

    return NewFunctionComponent
}