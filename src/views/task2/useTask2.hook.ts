import { AccountInviteTab } from 'models/accounts';
import { useState } from 'react';
import { NavItemProps } from 'rsuite';

export interface UseTask2Props {

}

export interface UseTask2Interface {
    nav_items: NavItemProps[]
    active: AccountInviteTab
    setActive: (value: AccountInviteTab) => void
}

export const useTask2 = (props: UseTask2Props): UseTask2Interface => {

    const [active, setActive] = useState<AccountInviteTab>('Accounts')
    const nav_items: NavItemProps[] = [
        {
            children: 'Accounts',
            onSelect: (eventKey?: string, e?: any) => { setActive('Accounts') }
        },
        {
            children: 'Invite',
            onSelect: (eventKey?: string, e?: any) => { setActive('Invite') }
        },
        {
            children: 'Invite Pending',
            onSelect: (eventKey?: string, e?: any) => { setActive('Invite Pending') }
        }
    ]

    return {
        nav_items,
        active,
        setActive,
    }
}