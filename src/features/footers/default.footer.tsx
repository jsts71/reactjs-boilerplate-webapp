import { FC } from "react";

export interface DefaultFooterProps{

}

export const DefaultFooter:FC<DefaultFooterProps> = (props:DefaultFooterProps) => {
    const {} = props;

    return(
        <div className="default-footer">default footer</div>
    )
}