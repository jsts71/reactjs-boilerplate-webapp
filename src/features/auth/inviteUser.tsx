import { FC, useState } from "react"
import { useInviteUser } from "./hooks/useInviteUser.hook";
import { Input, InputGroup } from "rsuite";
import ImportIcon from '@rsuite/icons/Import';

export interface InviteUserProps {

}

export const InviteUser: FC<InviteUserProps> = (props: InviteUserProps) => {

    const { nav_items, active, setActive } = useInviteUser({})

    const styles = {
        align: 'center',
        marginTop: '30px',
        padding: '20px',
    };

    return (
        <div>
            
            <i>Task pending . . .</i>
            <InputGroup size="md" style={styles}>
                <Input placeholder="Enter email for invitation" />
                <InputGroup.Button>
                    <ImportIcon onClick={() => { }} />
                </InputGroup.Button>
            </InputGroup>
        </div>
    )
}