import { AccountInviteTab } from 'models/accounts';
import { useState } from 'react';
import { NavItemProps } from 'rsuite';

export interface UseInviteUserProps {

}

export interface UseInviteUserInterface {
    nav_items: NavItemProps[]
    active: AccountInviteTab
    setActive: (value: AccountInviteTab) => void
}

export const useInviteUser = (props: UseInviteUserProps): UseInviteUserInterface => {

    const [active, setActive] = useState<AccountInviteTab>('Accounts')
    const nav_items: NavItemProps[] = [
        {
            children: 'Accounts',
            onSelect: (eventKey?: string, e?: any) => { setActive('Accounts') }
        },
        {
            children: 'Invite',
            onSelect: (eventKey?: string, e?: any) => { setActive('Invite') }
        },
        {
            children: 'Invite Pending',
            onSelect: (eventKey?: string, e?: any) => { setActive('Invite Pending') }
        }
    ]

    return {
        nav_items,
        active,
        setActive,
    }
}