import { Button, Col, IconButton, Modal, Row } from "rsuite";
import CloseIcon from '@rsuite/icons/Close';
import './style.scss'

export interface AlertModalProps {
    title: string,
    type: 'SUCCESS' | 'WARNING' | 'ERROR';
    description: string;
    open: boolean;
    setOpen: (value: boolean) => void;
}

const AlertModal = (props: AlertModalProps) => {

    const { title, type, description, open, setOpen } = props

    return (
        <>
            <Modal size="lg" open={open} backdrop="static" className="alert">
                <div className="alert-modal">
                    <Row className={`show-grid ${type}`}>
                        <Col xs={3} className="type">{type}</Col>
                        <Col xs={6} className="title">{title}</Col>
                        <Col xs={14} className="description">{description}</Col>
                        <Col xs={1} className="close"><IconButton icon={<CloseIcon />} onClick={() => setOpen(!open)} circle size="xs"/></Col>
                    </Row>
                </div>
            </Modal>
        </>
    )
}

export default AlertModal