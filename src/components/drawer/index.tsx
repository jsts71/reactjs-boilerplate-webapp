import React, { FC } from "react";
import { Button, Drawer, DrawerProps, Placeholder } from "rsuite";

import "./style.scss"

export interface DrawerComponentProps extends DrawerProps {
    open: boolean;
    setOpen: React.Dispatch<React.SetStateAction<boolean>>;

}

export const DrawerComponent: FC<DrawerComponentProps> = (props: DrawerComponentProps) => {

    const { placement, open, setOpen } = props

    return (
        <Drawer onClose={() => setOpen(false)} {...props}>
            <Drawer.Header>
                <Drawer.Title>Notifications</Drawer.Title>
                {/* <Drawer.Actions>
                    <Button onClick={() => setOpen(false)}>Cancel</Button>
                    <Button onClick={() => setOpen(false)} appearance="primary">
                        Confirm
                    </Button>
                </Drawer.Actions> */}
            </Drawer.Header>
            <Drawer.Body>
                <Placeholder.Paragraph rows={8} />
            </Drawer.Body>
        </Drawer>
    )
}